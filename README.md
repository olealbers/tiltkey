# TiltKey

This project helps sending keyboard entries when any joystick is moved over a defined offset.

I's main purpose is to detect analog movements from a Sidewinder USB-Controller and send those movements to a pinball-application as a "nudgte"-button (Pinball FX). But you might find other uses for that :)

## Configuring Tiltkey
After starting tiltkey you first should select your Game-Dvice you want to use:

![Game device select][select]

Immediatly you should see coordinates in the RAW-area. You now can change the offsets for the four directions. If one of those offsets are reached, the correspondig button gets red.
![red button][red]

When you are fine with your adjustments finally click onto the buttons to assign the key that should be sent:
![key][key]

## Let Tiltkey work
Simply start tiltkey.exe with the parameter /service
Now it will send the keys to any application that is listening. (Hint: Test with a simple application like "Notepad" before running your game to check the finetuning).

## Stop Tiltkey
Rerun tiltkey with parameter /stop (or kill the thread if you are brave enough :) 

[select]: ./pix/tilt1.png "Device select"
[red]: ./pix/tilt2.png "Detection"
[key]: ./pix/tilt3.png "Configure keys"