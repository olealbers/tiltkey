﻿using System;
using System.Drawing;
using System.Windows.Forms;
using SharpDX.DirectInput;
using TiltKey.Backend;

namespace TiltKey
{
    public partial class StickDisplay : Form
    {
        private DirectInput _directInput;
        private Joystick _joystick;
        private Timer _pollTimer = new Timer { Interval = 500 };
        private Quadrupel _keys = new Quadrupel();
        private int _checkInterval = 100;


        public StickDisplay()
        {
            InitializeComponent();

            _pollTimer.Tick += _pollTimer_Tick;
        }

        private void LoadSettings()
        {
            var settings = FileFunctions.LoadSettings(GetConfigFileName());
            _keys = settings.Keys;
            numTop.Value = settings.Offsets.Top;
            numBottom.Value = settings.Offsets.Bottom;
            numLeft.Value = settings.Offsets.Left;
            numRight.Value = settings.Offsets.Right;
            _checkInterval = settings.CheckInterval;
            foreach (var item in cmbStick.Items)
            {
                try
                {
                    var pad = (GamePad)item;
                    if (pad.Device.InstanceGuid == settings.JoystickId)
                    {
                        cmbStick.SelectedItem = item;
                    }
                }
                catch (Exception)
                {

                    throw;
                }
            }
        }

        private void _pollTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                JoystickState state = _joystick.GetCurrentState();
                lblRaw.Text = $"X:{state.X}\nY:{state.Y}";
                butTop.BackColor = state.Y <= numTop.Value ? Color.Red : SystemColors.Control;
                butBottom.BackColor = state.Y >= numBottom.Value ? Color.Red : SystemColors.Control;
                butLeft.BackColor = state.X >= numLeft.Value ? Color.Red : SystemColors.Control;
                butRight.BackColor = state.X <= numRight.Value ? Color.Red : SystemColors.Control;
            }
            catch (Exception)
            {
                tsStick.Text = "Error getting state. Timer stopped";
                _pollTimer.Stop();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            GetAllDevices();
            LoadSettings();
        }

        private int GetKeyPress()
        {
            var input = new KeyInput();
            input.ShowDialog();
            return (int)input.SelectedKey;
        }

        private void GetAllDevices()
        {
            cmbStick.Items.Clear();
            _directInput = new DirectInput();
            foreach (var gamePad in _directInput.GetDevices(DeviceClass.All, DeviceEnumerationFlags.AllDevices))
            {
                cmbStick.Items.Add(new GamePad(gamePad));
            }
        }

        private void cmbStick_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitStick();
        }

        private void InitStick()
        {
            _pollTimer.Stop();
            var gamepad = (GamePad)cmbStick.SelectedItem;
            try
            {
                _joystick = new Joystick(_directInput, gamepad.Device.InstanceGuid);
                _joystick.Acquire();
                _pollTimer.Start();
                tsStick.Text = $"{gamepad.Device.ProductName} will be tracked";
            }
            catch (Exception)
            {
                tsStick.Text = "Cannot aquire Stick. Is this really a joystick?";
            }
        }

        private void butTop_Click(object sender, EventArgs e)
        {
            _keys.Top = GetKeyPress();
        }

        private void butSave_click(object sender, EventArgs e)
        {
            FileFunctions.SaveSettings(GetConfigFileName(), new Settings
            {
                Keys = _keys,
                Offsets = new Quadrupel
                {
                    Top = (int)numTop.Value,
                    Bottom = (int)numBottom.Value,
                    Left = (int)numLeft.Value,
                    Right = (int)numRight.Value,
                },
                CheckInterval = _checkInterval,
                JoystickId = _joystick.Information.InstanceGuid

            });
            MessageBox.Show("Settings saved. Call this program with parameter '/service' to automatically send the keys");
        }

        private string GetConfigFileName()
        {
            return Application.StartupPath + "\\tiltkey.json";
        }

        private void butLeft_Click(object sender, EventArgs e)
        {
            _keys.Left = GetKeyPress();
        }

        private void butRight_Click(object sender, EventArgs e)
        {
            _keys.Right = GetKeyPress();
        }

        private void butBottom_Click(object sender, EventArgs e)
        {
            _keys.Bottom = GetKeyPress();
        }
    }
}
