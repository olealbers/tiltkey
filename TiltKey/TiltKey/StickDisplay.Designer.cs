﻿namespace TiltKey
{
    partial class StickDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cmbStick = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblRaw = new System.Windows.Forms.Label();
            this.grpTest = new System.Windows.Forms.GroupBox();
            this.butBottom = new System.Windows.Forms.Button();
            this.butRight = new System.Windows.Forms.Button();
            this.butLeft = new System.Windows.Forms.Button();
            this.butTop = new System.Windows.Forms.Button();
            this.numLeft = new System.Windows.Forms.NumericUpDown();
            this.numBottom = new System.Windows.Forms.NumericUpDown();
            this.numRight = new System.Windows.Forms.NumericUpDown();
            this.numTop = new System.Windows.Forms.NumericUpDown();
            this.butSave = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsStick = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1.SuspendLayout();
            this.grpTest.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTop)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Joystick;";
            // 
            // cmbStick
            // 
            this.cmbStick.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStick.FormattingEnabled = true;
            this.cmbStick.Items.AddRange(new object[] {
            "echt",
            "toll "});
            this.cmbStick.Location = new System.Drawing.Point(66, 18);
            this.cmbStick.Name = "cmbStick";
            this.cmbStick.Size = new System.Drawing.Size(269, 21);
            this.cmbStick.TabIndex = 1;
            this.cmbStick.SelectedIndexChanged += new System.EventHandler(this.cmbStick_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lblRaw);
            this.groupBox1.Location = new System.Drawing.Point(15, 224);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(309, 65);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Raw";
            // 
            // lblRaw
            // 
            this.lblRaw.Location = new System.Drawing.Point(6, 16);
            this.lblRaw.Name = "lblRaw";
            this.lblRaw.Size = new System.Drawing.Size(297, 32);
            this.lblRaw.TabIndex = 0;
            // 
            // grpTest
            // 
            this.grpTest.Controls.Add(this.butBottom);
            this.grpTest.Controls.Add(this.butRight);
            this.grpTest.Controls.Add(this.butLeft);
            this.grpTest.Controls.Add(this.butTop);
            this.grpTest.Controls.Add(this.numLeft);
            this.grpTest.Controls.Add(this.numBottom);
            this.grpTest.Controls.Add(this.numRight);
            this.grpTest.Controls.Add(this.numTop);
            this.grpTest.Location = new System.Drawing.Point(15, 61);
            this.grpTest.Name = "grpTest";
            this.grpTest.Size = new System.Drawing.Size(650, 157);
            this.grpTest.TabIndex = 3;
            this.grpTest.TabStop = false;
            this.grpTest.Text = "Test && Offsets";
            // 
            // butBottom
            // 
            this.butBottom.Location = new System.Drawing.Point(267, 97);
            this.butBottom.Name = "butBottom";
            this.butBottom.Size = new System.Drawing.Size(120, 23);
            this.butBottom.TabIndex = 13;
            this.butBottom.Text = "BOTTOM";
            this.butBottom.UseVisualStyleBackColor = true;
            this.butBottom.Click += new System.EventHandler(this.butBottom_Click);
            // 
            // butRight
            // 
            this.butRight.Location = new System.Drawing.Point(391, 70);
            this.butRight.Name = "butRight";
            this.butRight.Size = new System.Drawing.Size(120, 23);
            this.butRight.TabIndex = 12;
            this.butRight.Text = "RIGHT";
            this.butRight.UseVisualStyleBackColor = true;
            this.butRight.Click += new System.EventHandler(this.butRight_Click);
            // 
            // butLeft
            // 
            this.butLeft.Location = new System.Drawing.Point(132, 70);
            this.butLeft.Name = "butLeft";
            this.butLeft.Size = new System.Drawing.Size(120, 23);
            this.butLeft.TabIndex = 11;
            this.butLeft.Text = "LEFT";
            this.butLeft.UseVisualStyleBackColor = true;
            this.butLeft.Click += new System.EventHandler(this.butLeft_Click);
            // 
            // butTop
            // 
            this.butTop.Location = new System.Drawing.Point(267, 45);
            this.butTop.Name = "butTop";
            this.butTop.Size = new System.Drawing.Size(120, 23);
            this.butTop.TabIndex = 9;
            this.butTop.Text = "TOP";
            this.butTop.UseVisualStyleBackColor = true;
            this.butTop.Click += new System.EventHandler(this.butTop_Click);
            // 
            // numLeft
            // 
            this.numLeft.Location = new System.Drawing.Point(6, 73);
            this.numLeft.Maximum = new decimal(new int[] {
            65000,
            0,
            0,
            0});
            this.numLeft.Name = "numLeft";
            this.numLeft.Size = new System.Drawing.Size(120, 20);
            this.numLeft.TabIndex = 8;
            // 
            // numBottom
            // 
            this.numBottom.Location = new System.Drawing.Point(267, 126);
            this.numBottom.Maximum = new decimal(new int[] {
            65000,
            0,
            0,
            0});
            this.numBottom.Name = "numBottom";
            this.numBottom.Size = new System.Drawing.Size(120, 20);
            this.numBottom.TabIndex = 7;
            // 
            // numRight
            // 
            this.numRight.Location = new System.Drawing.Point(517, 73);
            this.numRight.Maximum = new decimal(new int[] {
            65000,
            0,
            0,
            0});
            this.numRight.Name = "numRight";
            this.numRight.Size = new System.Drawing.Size(120, 20);
            this.numRight.TabIndex = 6;
            // 
            // numTop
            // 
            this.numTop.Location = new System.Drawing.Point(267, 19);
            this.numTop.Maximum = new decimal(new int[] {
            65000,
            0,
            0,
            0});
            this.numTop.Name = "numTop";
            this.numTop.Size = new System.Drawing.Size(120, 20);
            this.numTop.TabIndex = 5;
            // 
            // butSave
            // 
            this.butSave.Location = new System.Drawing.Point(590, 21);
            this.butSave.Name = "butSave";
            this.butSave.Size = new System.Drawing.Size(75, 23);
            this.butSave.TabIndex = 4;
            this.butSave.Text = "Save";
            this.butSave.UseVisualStyleBackColor = true;
            this.butSave.Click += new System.EventHandler(this.butSave_click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsStick});
            this.statusStrip1.Location = new System.Drawing.Point(0, 299);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(678, 22);
            this.statusStrip1.TabIndex = 5;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsStick
            // 
            this.tsStick.Name = "tsStick";
            this.tsStick.Size = new System.Drawing.Size(144, 17);
            this.tsStick.Text = "Please select your Joystick";
            // 
            // StickDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(678, 321);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.butSave);
            this.Controls.Add(this.grpTest);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cmbStick);
            this.Controls.Add(this.label1);
            this.Name = "StickDisplay";
            this.Text = "TiltKey";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.grpTest.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numTop)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbStick;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblRaw;
        private System.Windows.Forms.GroupBox grpTest;
        private System.Windows.Forms.NumericUpDown numLeft;
        private System.Windows.Forms.NumericUpDown numBottom;
        private System.Windows.Forms.NumericUpDown numRight;
        private System.Windows.Forms.NumericUpDown numTop;
        private System.Windows.Forms.Button butSave;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tsStick;
        private System.Windows.Forms.Button butTop;
        private System.Windows.Forms.Button butBottom;
        private System.Windows.Forms.Button butRight;
        private System.Windows.Forms.Button butLeft;
    }
}

