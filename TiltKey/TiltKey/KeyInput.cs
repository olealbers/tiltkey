﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace TiltKey
{
    public partial class KeyInput : Form
    {
        [DllImport("user32.dll")]
        private static extern short GetAsyncKeyState(Keys key);
        public Keys SelectedKey { get; set; }


        public KeyInput()
        {
            InitializeComponent();
        }

        private void KeyInput_KeyDown(object sender, KeyEventArgs e)
        {
            Keys lastKey = Keys.None;
            foreach (var key in Enum.GetValues(typeof(Keys)))
            {
                var keyValue = (Keys)key;
                if (GetAsyncKeyState(keyValue) < 0)
                {
                    lastKey = keyValue;
                }
            }
            MessageBox.Show($"'{lastKey.ToString()} ({(int)lastKey})' detected");
            SelectedKey = lastKey;
            this.Close();
        }
    }
}
