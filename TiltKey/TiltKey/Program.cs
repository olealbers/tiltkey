﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using TiltKey.Backend;

namespace TiltKey
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length == 1 && args[0] == "/service")
            {
                StartAsService();
            }
            else
            {

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new StickDisplay());
            }
        }

        private static void StartAsService()
        {
            var settings = FileFunctions.LoadSettings(Application.StartupPath + "\\tiltkey.json");
            new Live(settings);

        }
    }
}
