﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using SharpDX.DirectInput;
using static TiltKey.Backend.Constants;
using T = System.Timers;

namespace TiltKey.Backend
{
    public class Live
    {

        [DllImport("user32.dll", SetLastError = true)]
        private static extern uint SendInput(uint numberOfInputs, INPUT[] inputs, int sizeOfInputStructure);

        private static void SendKeyDown(ushort keyCode)
        {
            var input = new KEYBDINPUT
            {
                Vk = keyCode
            };

            SendKeyboardInput(input);
        }

        private static void SendKeyUp(ushort keyCode)
        {
            var input = new KEYBDINPUT
            {
                Vk = keyCode,
                Flags = 2
            };
            SendKeyboardInput(input);
        }

        private static void SendKeyboardInput(KEYBDINPUT keybInput)
        {
            INPUT input = new INPUT
            {
                Type = 1
            };
            input.Data.Keyboard = keybInput;

            if (SendInput(1, new[] { input }, Marshal.SizeOf(typeof(INPUT))) == 0)
            {
                throw new Exception();
            }
        }

        private T.Timer _timer;
        private Joystick _joystick;
        private Settings _settings;

        public Live(Settings settings)
        {

            _settings = settings;
            _timer = new T.Timer(settings.CheckInterval);
            _joystick = new Joystick(new DirectInput(), settings.JoystickId);
            _joystick.Acquire();
            _timer.Elapsed += _timer_Elapsed;
            Init();
        }



        private void Init()
        {
            var waitHandle = new EventWaitHandle(false, EventResetMode.AutoReset, "TiltKey", out bool createdNew);
            if (!createdNew)
            {
                // Only start ONCE
                Console.WriteLine("Already running");
                waitHandle.Set();
                return;
            }
            var signaled = false;
            _timer.Start();

            do
            {
                signaled = waitHandle.WaitOne(TimeSpan.FromSeconds(5));
            } while (!signaled);
        }


        private void CheckTilt(int currentPosition, int minValue, int maxValue, int key)
        {
            if (key == 0) return;
            if (currentPosition < minValue || currentPosition > maxValue)
            {
                SendKeyDown((ushort)key);


                System.Threading.Thread.Sleep(_settings.DelayAfterDetection * 1000);
                SendKeyUp((ushort)key);
            }
        }

        private void _timer_Elapsed(object sender, T.ElapsedEventArgs e)
        {
            _timer.Stop();
            try
            {
                JoystickState state = _joystick.GetCurrentState();
                CheckTilt(state.Y, _settings.Offsets.Top, int.MaxValue, _settings.Keys.Top);
                CheckTilt(state.Y, 0, _settings.Offsets.Bottom, _settings.Keys.Top);
                CheckTilt(state.X, _settings.Offsets.Right, int.MaxValue, _settings.Keys.Top);
                CheckTilt(state.X, 0, _settings.Offsets.Left, _settings.Keys.Top);
            }
            catch (Exception)
            {
            }
            finally
            {

                _timer.Start();
            }
        }
    }

}
