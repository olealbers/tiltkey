﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharpDX.DirectInput;

namespace TiltKey.Backend
{
    public class GamePad
    {
        public DeviceInstance Device { get;  }
        public GamePad(DeviceInstance deviceInstance)
        {
            Device = deviceInstance;
        }


        public override string ToString()
        {
            return Device.ProductName;
        }

    }
}
