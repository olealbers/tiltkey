﻿using System;

namespace TiltKey.Backend
{
    public class Settings
    {
        public Quadrupel Offsets { get; set; }
        public Quadrupel Keys { get; set; }
        public int CheckInterval { get; set; } = 100;   // Interval in ms
        public int DelayAfterDetection { get; set; } = 10;
        public Guid JoystickId { get; set; }

    }

    public class Quadrupel
    {
        public int Left { get; set; }
        public int Right { get; set; }
        public int Top { get; set; }
        public int Bottom { get; set; }
    }

}
