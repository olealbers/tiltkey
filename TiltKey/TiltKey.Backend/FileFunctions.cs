﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TiltKey.Backend
{
    public static class FileFunctions
    {
        public static void SaveSettings(string filename, Settings settings)
        {
            System.IO.File.WriteAllText(filename, JsonConvert.SerializeObject(settings, Formatting.Indented),Encoding.UTF8);
        }

        public static Settings LoadSettings(string filename)
        {
            return JsonConvert.DeserializeObject<Settings>(System.IO.File.ReadAllText(filename, Encoding.UTF8));
        }
    }
}
